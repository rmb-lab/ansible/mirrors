# mirrors

Ansible to setup Linux Mirrors on FreeNAS jail

## Setup

1. Create a jail called `mirrors.rmb938.me` with an IP of `192.168.23.41`
1. Open a shell and run the following:
    1. `pkg install bash python`
    1. `adduser`
        * Username: `jail-user`
        * Full name: `jail-user`
        * Uid: `1002`
        * Other Groups: `wheel`
        * Random Password
    1. `sysrc sshd_enable="YES"`
    1. `service sshd start`
1. Run `ssh-copy-id jail-user@mirrors.rmb938.me` locally
1. SSH in `ssh jail-user@mirrors.rmb938.me`
